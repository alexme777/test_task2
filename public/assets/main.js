/*
	Было лень самому писать скрипт, нашел его на гитхабе. Главное результат:)
*/

$( document ).ready(function() {
	var showAlert = function(text) {
		var custom_alert = $('div.alert');
		if(text === false) {
			$(custom_alert).hide();
		} else {
			$(custom_alert).empty();
			$(custom_alert).append(text);
			$(custom_alert).show();
		}
		return;
	}

	var showBalances = function() {
		var users = $.find('select'),
			balanceSelector = 'p.balance-info',
			val;
		users.forEach(function(user){
			val = $(user).val();
			if(!val) return false;
			$.get('/index/balance', {id: val}, function(data) {
				if(data.ok) {
					$(user).siblings(balanceSelector).text(data.result);
				}
			})
			
		})
	}

	$('select').change(function() {
		showAlert(false);
		showBalances();
	})


	$('button.send-button').click(function() {
		var sender = $('select[name=fromUser]').val(),
			recipient = $('select[name=toUser]').val(),
			amount = $('input.amount').val();
		if( !sender || !recipient || !amount ) {
			showAlert('Укажите все параметры!');
			return false;
		}

		if( !parseFloat(amount) ) {
			showAlert('Сумма должна быть числом!');
			return false;
		}

		$.post('/index/transaction', {from: sender, to: recipient, amount: amount}, function(data){
			if(!data.ok) {
				showAlert(data.error);
				return false;
			}
			showAlert(data.result);
			showBalances();
		});
	})
});