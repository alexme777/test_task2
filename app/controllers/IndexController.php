<?php
namespace APP\Controller;

use APP\Controller\ControllerBase;
use APP\Model\Users;
use APP\Model\Transaction;
use Phalcon\Mvc\Dispatcher\Exception;
use Phalcon\Http\Response;
use Phalcon\Http\Request;

class IndexController extends ControllerBase
{
	
    public function indexAction()
    {

    }
	
	public function balanceAction() 
	{
	
		$request = new Request();
		$response = new Response();
		
        if($request->isAjax() != true) 
		{
            throw new Exception('Неверный запрос');
        }
		
		$id = (int) $request->get('id');
		
		$user = Users::findFirst($id);
		
        if( !($id) or !($user) ) 
		{
			$response->setJsonContent(
				[
					'ok' => false, 
					'error' => 'Неверный запрос'
				]
			);
        } 
		else {
			
			$response->setJsonContent(
				[
					'ok' => true,
					'result' => "Баланс {$user->balance}"
				]
			);
		}
		
		return $response;
    }


    public function transactionAction() 
	{
		
		$request = $this->request;
		$response = new Response();
		
        if($request->isAjax() != true) 
		{
            throw new Exception('Неверный запрос');
        }
		
		$params = $request->getPost();
	
        $transaction = new Transaction;

		$transaction->setFrom($params['from']);
		$transaction->setTo($params['to']);
		$transaction->setAmount($params['amount']);
		
        $transaction->execute();
		
		$messages = $transaction->getMessages();
	
        if(is_array($messages) && count($messages) > 0) 
		{		
			$errorMessage = "";
			
			foreach ($messages as $message) {
				$errorMessage .= $message->getMessage()."<br/>";
			}
			
			$response->setJsonContent(
				[
					'ok' => false,
					'error' => $errorMessage
				]
			);
        } 
		else 
		{
            $fromName = Users::findFirst($transaction->from)->name;
            $toName = Users::findFirst($transaction->to)->name;

			$response->setJsonContent(
				[
					'ok' => true,
					'result' => "Сумма {$transaction->amount} от {$fromName} для {$toName} переведена."
				]
			);
        }

        return $response;
    }
}

