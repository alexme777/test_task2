<?php 
namespace APP\Model;

use APP\Model\Users;
use Phalcon\Validation;
use Phalcon\Mvc\Model\Message as Message;

class Transaction extends \Phalcon\Mvc\Model 
{

    protected $from;
    protected $to;
    protected $amount;
	
    public function execute() 
	{
		$prop = array();
	
		$prop["from"] = $this->getFrom();
		$prop["to"] = $this->getTo();
		$prop["amount"] = $this->getAmount();
		
        if($prop["from"] == $prop["to"]) 
		{
			$message = new Message('Невозможно отправить сумму одному пользователю');
            $this->appendMessage($message);
			return false;
        }

		$from = Users::findFirst($prop["from"]);			
		$to = Users::findFirst($prop["to"]);
		
		if($from->balance < $this->amount) 
		{
			$message = new Message('Недостаточно средств');
			$this->appendMessage($message);
			return false;
		}

		$from->balance -= $prop["amount"];

		$to->balance += $prop["amount"];
		
		$from->save();
		$to->save();

		return true;
    }
	
	public function setFrom($from)
	{
		$this->from = null;
		
		if((int) $from > 0)
		{
			return $this->from = $from;
		}
		else
		{
			$message = new Message('Выберите отправителя');
            $this->appendMessage($message);
			return false;
		}
	}
	
	public function setTo($to)
	{
		$this->to = null;
		
		if((int) $to > 0)
		{
			return $this->to = $to;
		}
		else
		{
			$message = new Message('Выберите получателя');
            $this->appendMessage($message);
			return false;
		}
	}
	
	public function setAmount($amount)
	{
		$this->amount = null;
		
		if(is_numeric($amount) && $amount > 0)
		{
			return $this->amount = $amount;
		}
		else
		{
			$message = new Message('Сумма должна быть числом!');
            $this->appendMessage($message);
			return false;
		}
	}
	
	public function getFrom()
	{
		if($this->from)
		{
			return $this->from;
		}
	}
	
	public function getTo()
	{
		if($this->to)
		{
			return $this->to;
		}
		else
		{
			return false;
		}
	}
	
	public function getAmount()
	{
		if($this->amount)
		{
			return $this->amount;
		}
		else
		{
			return false;
		}
	}

}