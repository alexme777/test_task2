<?php 
namespace Test\Model;

use APP\Model\Transaction;
use APP\Model\Users;

class TransactionTest extends \UnitTestCase
{
	public function testTransaction()
	{

		$modelTransaction = new Transaction();
		
		$from = Users::findFirst(1);
		
		$this->assertEquals(1, $modelTransaction->setFrom(1));
		$this->assertEquals(2, $modelTransaction->setTo(2));
		$this->assertEquals(1.00, $modelTransaction->setAmount(1.00));
		
		$this->assertEquals(1, $modelTransaction->getFrom());
		$this->assertEquals(2, $modelTransaction->getTo());
		$this->assertEquals(1.00, $modelTransaction->getAmount());
		
		if(Users::findFirst(1) && Users::findFirst(2))
		{
			$this->assertEquals(true, $modelTransaction->execute());
		}
		
		$this->assertEquals(1, $modelTransaction->setFrom(1));
		$this->assertEquals(1, $modelTransaction->setTo(1));
		
		if(Users::findFirst(1))
		{
			$this->assertEquals(false, $modelTransaction->execute());
		}
		
		$amount = $from->balance + 1; 
		
		if($from->balance < $amount) 
		{
			$this->assertEquals(false, $modelTransaction->execute());
		}
	}	
}