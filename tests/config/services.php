<?php
use Phalcon\DI;
use Phalcon\Mvc\Model\Manager as ModelsManager;
use Phalcon\Mvc\Model\Metadata\Memory as MetaData;
/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */

 $di = Di::getDefault();
		
		$di->set(
			"modelsManager",
			function() {
				$modelsManager = new ModelsManager();
				return $modelsManager;
			}
		);
		
		$di->set(
			'modelsMetadata',
			new MetaData()
		);
		
		$di->set('db', function() use ($config) {
			return new \Phalcon\Db\Adapter\Pdo\Mysql(array(
				"host" => $config->database->host,
				"username" => $config->database->username,
				"password" => $config->database->password,
				"dbname" => $config->database->name
			));
		});


