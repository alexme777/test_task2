<?php
use Phalcon\Loader;

$loader = new Loader();

$loader->registerDirs(
    [
        ROOT_PATH,
		$config->application->controllersDir,
		$config->application->modelsDir,
    ]
);

$loader->registerNamespaces(
	array(
		'APP\Controller' => $config->application->controllersDir,
		'APP\Model' 	 => $config->application->modelsDir,
	)
);

$loader->register();

