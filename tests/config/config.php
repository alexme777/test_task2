<?php

return new \Phalcon\Config(array(
	'database' => array(
		'adapter'  => 'Mysql',
		'host'     => 'localhost',
		'username' => 'root',
		'password' => '123456',
		'name'     => 'test_task',
	),
	'application' => array(
		'controllersDir' => APP_PATH . '/controllers/',
        'modelsDir'      => APP_PATH . '/models/',
		'viewsDir'       => APP_PATH . '/views/',
		'libraryDir'     => APP_PATH . '/library/',
		'baseUri'        => '/public/',
	)
));
