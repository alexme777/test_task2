<?php
use Phalcon\Loader;
use APP\Model\Transaction;
use Phalcon\DI;
use Phalcon\Test\UnitTestCase as PhalconTestCase;
use Phalcon\Mvc\Model\Manager as ModelsManager;
use Phalcon\Mvc\Model\Metadata\Memory as MetaData;

ini_set('display_errors',1);
error_reporting(E_ALL);

define("ROOT_PATH", __DIR__);
define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');
define('TEST_PATH', BASE_PATH . '/tests');

$config  = include_once(TEST_PATH . '/config/config.php');


include __DIR__ . "/../vendor/autoload.php";

/**
 * Include loader
 */
require TEST_PATH . '/config/loader.php';


